<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail,DB,Request,Cart;
use App\Http\Requests\RegisterRequest;

class ShopController extends Controller
{
    function index(){
        
        return view('shop.pages.home');
    }
    // Trang category
    function category($id = 1){
        return view('shop.pages.category');
    }
    // Trang detail
    function detail($id = 1){
        return view('shop.pages.detail');
    }
    // Trang cart
    function cart(){
        return view('shop.pages.cart');
    }
    // Trang account
    function account(){
        return view('shop.pages.account');
    }
    // Trang contact
    function contact(){
        return view('shop.pages.contact');
    }
    // Trang listing
    function listing(){
        return view('shop.pages.listing');
    }
    // Trang about
    function about(){
        return view('shop.pages.about');
    }

    // Trang login
    function login(){
        return view('shop.pages.login');
    }
    // Trang register
    function register(){
        $city = DB::table('eli_city')->get();
        // echo '<pre>';
        // var_dump($city);
        return view('shop.pages.register', compact('city'));
    }
    function post_register(RegisterRequest $request){
        echo 1;
    }
    function load_district($city){
        if(Request::ajax()){
            $district = DB::table('eli_district')->where('id_city',$city)->get();
            foreach ($district as $key => $value) {
                echo '<option value="'.$value->id.'">'.$value->name.'</option>';
            }
        }
    }
    
}
