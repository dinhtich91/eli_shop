<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| 		----------- BACK END -----------------
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix'=>'admin'], function(){
	Route::group(['prefix' => 'cate'], function(){
		Route::get('add', ['as' => 'admin.cate.add', 'uses' => 'CategoryController@add']);
	});
});	


/*
|--------------------------------------------------------------------------
| 		----------- FONT END -----------------
|--------------------------------------------------------------------------
|
*/
// Trang chủ load home
Route::get('/', ['as' => 'home', 'uses' => 'ShopController@index']);
// Trang category
Route::get('category/{id?}', ['as' => 'category', 'uses' => 'ShopController@category']);
// Trang detail
Route::get('detail/{id?}', ['as' => 'detail', 'uses' => 'ShopController@detail']);
// Trang cart
Route::get('cart', ['as' => 'cart', 'uses' => 'ShopController@cart']);
// Trang account
Route::get('account', ['as' => 'account', 'uses' => 'ShopController@account']);
// Trang contact
Route::get('contact', ['as' => 'contact', 'uses' => 'ShopController@contact']);
// Trang listing
Route::get('listing', ['as' => 'listing', 'uses' => 'ShopController@listing']);
// Trang about
Route::get('about', ['as' => 'about', 'uses' => 'ShopController@about']);

// Trang login
Route::get('login', ['as' => 'login', 'uses' => 'ShopController@login']);
// Trang register
Route::get('register', ['as' => 'register', 'uses' => 'ShopController@register']);
Route::post('register-post', ['as' => 'post_register', 'uses' => 'ShopController@post_register']);
// Trang tải quận huyện
Route::get('load_district/{city}', ['as' => 'load_district', 'uses' => 'ShopController@load_district']);


